from clearml import Dataset

import folder.folder as folder
import folder.sub_folder.sub_folder as sub_folder

import argparse
import os
import shutil


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_id', type=str, default='99bd3283a0e44c9d8e9f5bb71af9eac7', help='Dataset ID')
    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    dataset_id = args.dataset_id
    print(f'Got Dataset ID: {dataset_id}')

    dataset = Dataset.get(dataset_id)
    print(f'Got Dataset: {dataset}, now downloading...')

    location = dataset.get_local_copy()
    print(f'Got local copy at: {location}')

    print(os.listdir(location))

    print('Removing local copy...')
    shutil.rmtree(location)

if __name__ == '__main__':
    # main()
    folder.fun()
    sub_folder.fun()